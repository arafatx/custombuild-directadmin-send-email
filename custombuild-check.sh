#/bin/sh - Directadmin custombuild update checker

function cb_checkupdate()
{

/bin/echo "================================================================"
/bin/echo "[custombuild] Directadmin Custombuild is checking system update.."
/bin/echo "[custombuild] Please wait..."
cd /usr/local/directadmin/custombuild

#GLOBAL VARS
UPDATE_BODY="da-cbuild-update.log"
WARNING_STATUS="N/A"
MYEMAIL="webmaster@sofibox.com"
MYHOSTNAME=`/bin/hostname`
AVAIL_UPDATES_COUNT="`./build versions_nobold | grep -c -e 'update is available.'`"

/bin/echo "Custombuild checked on `date`" > /tmp/$UPDATE_BODY
/bin/echo "=============================" >> /tmp/$UPDATE_BODY
/bin/echo "" >> /tmp/$UPDATE_BODY
./build update >/dev/null 2>&1

#/bin/echo $AVAIL_UPDATES
if [ "${AVAIL_UPDATES_COUNT}" -gt 0 ]; then
  WARNING_STATUS="WARNING"
  if [ "${AVAIL_UPDATES_COUNT}" -eq 1 ]; then
  /bin/echo "$AVAIL_UPDATES_COUNT software update is available:" >> /tmp/$UPDATE_BODY
  elif [ "${AVAIL_UPDATES_COUNT}" -ge 2 ]; then
  /bin/echo "$AVAIL_UPDATES_COUNT software updates are available:" >> /tmp/$UPDATE_BODY
  fi
  /bin/echo "" >> /tmp/$UPDATE_BODY
 ./build versions_nobold | grep -e 'update is available.' >> /tmp/$UPDATE_BODY
  /bin/mail -s "[custombuild][$WARNING_STATUS|$AVAIL_UPDATES_COUNT] Directadmin Custombuild System Update Check Report $MYHOSTNAME" $MYEMAIL < /tmp/$UPDATE_BODY
  else
  /bin/echo "[custombuild] No software update is available. Thank you" >> /tmp/$UPDATE_BODY
  WARNING_STATUS="OK"
  fi
/bin/echo "[custombuild] Check status: $WARNING_STATUS"
#/bin/mail -s "[custombuild][$WARNING_STATUS|$AVAIL_UPDATES_COUNT] Directadmin Custombuild System Update Check Report $MYHOSTNAME" $MYEMAIL < /tmp/$UPDATE_BODY
/bin/echo "[custombuild] Done checking system. Email is set to be sent to $MYEMAIL"
/bin/echo "================================================================"
/bin/rm -rf /tmp/$UPDATE_BODY
exit 0;
}

##############
#Function call:

cb_checkupdate